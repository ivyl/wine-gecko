#!/bin/bash

set -e

case x"$1" in
x-h|x--help|xhelp)
    echo "Usage: wine/make_package [-dbg] [-win32] [-win64] [-pdb] [--no-build] [--force-build] [--package-only] [--msi-package] [--tar-package] [--no-package] [--disable-strip] [--unstripped]"
    exit 0
    ;;
esac

if [ ! -e client.mk ]
then
    echo "Script must be ran from top source directory"
    exit 1
fi

target=i686-w64-mingw32
target_abi=win32
target_arch=x86
pkg_type=msi
do_strip=yes
no_xulinstaller=no
pdb=no

while [ "$1" != "" ]
do
    case $1 in
    -dbg)
        debug="-dbg"
        pkg_type=tar
        do_strip=no
        ;;
    -win32)
        target=i686-w64-mingw32
        target_abi=win32
        target_arch=x86
        ;;
    -win64)
        target=x86_64-w64-mingw32
        target_abi=win64
        target_arch=x86_64
        ;;
    -pdb)
        do_strip=no
        pdb=yes
        ;;
    --no-build)
        no_build=yes
        ;;
    --force-build)
        force_build=yes
        ;;
    --package-only)
        no_build=yes
        no_xulinstaller=yes
        ;;
    --msi-package)
        pkg_type=msi
        ;;
    --tar-package)
        pkg_type=tar
        ;;
    --all-package)
        pkg_type=all
        ;;
    --no-package)
        pkg_type=none
        ;;
    --disable-strip)
        do_strip=no
        ;;
    --unstripped)
        pkg_type=unstripped
        ;;
    *)
        echo "make_package: Unknown option: $1"
        exit 1
        ;;
    esac
    shift
done

src_dir=$(pwd)
version=$(head -n 1 $src_dir/wine/VERSION)
wine_gecko_dir=wine-gecko-$version-$target_arch
dist_basename=$wine_gecko_dir$debug

if [ "$OS" = "Windows_NT" ]; then
    package_basename=${dist_basename}-msvc
else
    package_basename=${dist_basename}
fi

if [ "$BUILD_DIR" = "" ]; then
    build_dir=$src_dir/../$dist_basename
else
    build_dir="$BUILD_DIR"
fi

if [ "$force_build" = "yes" ]
then
    echo "make_package: Removing build directory..."
    rm -rf $build_dir
fi

if [ "$no_build" != "yes" ]
then
    echo "make_package: Building firefox..."

    config_file="$(mktemp)"

    mkdir -p $build_dir

    case $OS in
        Windows_NT)
            build_dir="$(cd $build_dir && pwd -W)"
            ;;
        *)
            echo export CROSS_COMPILE=1 >>$config_file
            echo "ac_add_options --target=$target" >>$config_file
            ;;
    esac

    echo "mk_add_options MOZ_OBJDIR=\"$build_dir\"" >>$config_file
    echo "mk_add_options MOZ_MAKE_FLAGS=\"$MAKEOPTS\"" >>$config_file

    if [ "$debug" = "" ]
    then
        echo "ac_add_options --disable-debug" >>$config_file
        echo "ac_add_options --enable-optimize" >>$config_file
    else
        echo "ac_add_options --enable-debug" >>$config_file
        echo "ac_add_options --disable-optimize" >>$config_file
    fi

    if [ "$pdb" = "yes" ]
    then
        echo "ac_add_options --enable-pdb" >>$config_file
    fi

    if [ "$do_strip" = "no" ]
    then
        echo "ac_add_options --disable-install-strip" >>$config_file
    fi

    cat wine/mozconfig-common >>$config_file

    if cmp -s $config_file $build_dir/mozconfig; then
        echo "mozconfig is unchanged"
        rm -f $config_file
    else
        mv $config_file $build_dir/mozconfig
        MOZCONFIG=$build_dir/mozconfig ./mach configure
    fi

    MOZCONFIG=$build_dir/mozconfig make $MAKEOPTS -C $build_dir
fi

echo "make_package: Preparing Wine Gecko files..."
rm -rf "$build_dir/dist/$wine_gecko_dir"

if [ "$no_xulinstaller" != "yes" ]
then
    MOZCONFIG=$build_dir/mozconfig make $MAKEOPTS -C $build_dir/browser/installer
fi

cp -r "$build_dir/dist/firefox" "$build_dir/dist/$wine_gecko_dir"
echo -n "Wine Gecko $version" > "$build_dir/dist/$wine_gecko_dir/VERSION"

while read -r f; do
    rm -f "$build_dir/dist/$wine_gecko_dir/$f"
done <wine/remove-files.txt

case $pkg_type in
msi|all)
    echo "make_package: Creating MSI file..."

    msi_dir=$build_dir/dist/msi
    rm -rf $msi_dir
    mkdir $msi_dir

    cd $build_dir/dist

    datetime=`date "+%Y\/%m\/%d %k:%m:%S"`
    keypath=`find $wine_gecko_dir -mindepth 1 -maxdepth 1 -type f | sort | head -1 | cut -d'/' -f2- | sed -e "s/\//!/g"`
    cabfile=winegecko.cab

    if [ "$target_abi" = "win64" ]
    then
        msi_sysdir=System64Folder
        msi_productname="Wine Gecko (64-bit)"
        msi_targetcpu="x64;0"
        msi_productcode=$(head -n 3 $src_dir/wine/VERSION |tail -n 1)
        msi_compattr="256"
    else
        msi_sysdir=SystemFolder
        msi_productname="Wine Gecko (32-bit)"
        msi_targetcpu="Intel;0"
        msi_productcode=$(head -n 2 $src_dir/wine/VERSION |tail -n 1)
        msi_compattr="0"
    fi

    mkdir $msi_dir/tables
    for f in $src_dir/wine/tables/*.in
    do
        filename=$(basename $f)
        cat $src_dir/wine/tables/$filename \
            |sed -e "s/@VERSION@/$version/g" \
            |sed -e "s/@KEYPATH@/$keypath/g" \
            |sed -e "s/@DATETIME@/$datetime/g" \
            |sed -e "s/@SYSTEMFOLDER@/$msi_sysdir/g" \
            |sed -e "s/@PRODUCTCODE@/$msi_productcode/g" \
            |sed -e "s/@PRODUCTNAME@/$msi_productname/g" \
            |sed -e "s/@TARGETCPU@/$msi_targetcpu/g" \
            |sed -e "s/@COMPATTR@/$msi_compattr/g" \
            > $msi_dir/tables/$(echo $filename | sed 's/.in$//')
    done

    msi_tmp=`mktemp`

    process_dirs()
    {
        depth=$1
        find $wine_gecko_dir -mindepth $depth -maxdepth $depth -type d | sort > $msi_tmp

        if [ ! -s $msi_tmp ]; then
            return
        fi

        for d in `cat $msi_tmp`;
        do
            key=`echo $d | cut -d'/' -f2- | sed -e "s/\//|/g"`
            guid=`uuidgen | tr [a-z] [A-Z]`

            keypath=`find $d -mindepth 1 -maxdepth 1 -type f | sort | head -1 | cut -d'/' -f2- | sed -e "s/\//!/g"`
            if [ "x$keypath" != "x" ]; then
                echo -e $key\\t\{$guid\}\\t$key\\t0\\t\\t$keypath >>$msi_dir/tables/component.idt
            fi

            if [ $depth = "1" ]; then
                parent=WINEGECKODIR
            else
                parent=`dirname $d | cut -d '/' -f2- | sed -e "s/\//|/g"`
            fi
            default=`basename $d`
            echo -e $key\\t$parent\\t$default >>$msi_dir/tables/directory.idt
        done

        depth=`expr $depth + 1`
        process_dirs $depth
    }

    process_dirs 1

    find $wine_gecko_dir -mindepth 1 -type d | sort > $msi_tmp

    for d in `cat $msi_tmp`;
    do
        component=`echo $d | cut -d '/' -f2- | sed -e "s/\//|/g"`
        hasfile=`find $d -mindepth 1 -maxdepth 1 -type f`
        if [ "x$hasfile" != "x" ]; then
            echo -e wine_gecko\\t$component >> $msi_dir/tables/featurecomponents.idt
        fi
    done

    find $wine_gecko_dir -type f | sort > $msi_tmp

    sequence=1
    for f in `cat $msi_tmp`;
    do
        key=`echo $f | cut -d '/' -f2- | sed -e "s/\//!/g"`
        basename=`basename $f`
        dirname=`dirname $f`
        if [ $dirname = $wine_gecko_dir ];
        then
            component=gecko
        else
            component=`echo $dirname | cut -d '/' -f2- | sed -e "s/\//|/g"`
        fi
        filesize=`ls -l $f | awk '{ print $5 }'`
        echo -e $key\\t$component\\t$basename\\t$filesize\\t\\t\\t\\t$sequence >> $msi_dir/tables/file.idt
        sequence=`expr $sequence + 1`
    done

    find $wine_gecko_dir -type f -print | sort > $msi_tmp

    mkdir $msi_dir/files
    for f in `cat $msi_tmp`;
    do
        key=`echo $f | cut -d '/' -f2- | sed -e "s/\//!/g"`
        ln -s ../../$f $msi_dir/files/$key
    done

    num_files=`find $wine_gecko_dir -type f | wc -l`
    echo -e 1\\t$num_files\\t\\t\#$cabfile\\t\\t >> $msi_dir/tables/media.idt

    rm -f $msi_tmp

    if test -z "$WINE_BUILD_DIR"
    then
        WINE=wine
        WINESERVER=wineserver
    else
        WINE=$WINE_BUILD_DIR/wine
        WINESERVER=$WINE_BUILD_DIR/server/wineserver
    fi

    if test -z "$WINEPREFIX"
    then
        tmp_wineprefix=`mktemp -d`
        export WINEDLLOVERRIDES=mshtml=
        export WINEPREFIX=$tmp_wineprefix
    fi

    rm -f $cabfile $package_basename.msi

    $WINE cabarc.exe -r -m mszip N $cabfile msi/files

    $WINE winemsibuilder -i $package_basename.msi msi/tables/*
    $WINE winemsibuilder -a $package_basename.msi $cabfile $cabfile

    if test -n "$tmp_wineprefix"
    then
        $WINESERVER -k
        rm -rf $tmp_wineprefix
    fi

    echo Package done:
    echo $(pwd)/$package_basename.msi
    ;;
esac

case $pkg_type in
tar|all)
    echo "make_package: Creating .tar.xz archive..."

    cd $build_dir/dist

    tar -cJf $package_basename.tar.xz $wine_gecko_dir

    echo Package done:
    echo $(pwd)/$package_basename.tar.xz

    if [ "$pdb" != "no" ]
    then
        echo "make_package: Creating PDB archive"

        # FIXME: ideally we'd get that from build system
        cd ..
        for x in `find dist/$wine_gecko_dir/ -name \*.dll` ; do y=`basename $x .dll` ; find $(ls |grep -v dist) -name $y.pdb -exec cp {} dist/$wine_gecko_dir/ \; ; done
        for x in `find dist/$wine_gecko_dir/ -name \*.exe` ; do y=`basename $x .exe` ; find $(ls |grep -v dist) -name $y.pdb -exec cp {} dist/$wine_gecko_dir/ \; ; done
        cd dist

        tar -cJf $package_basename-pdb.tar.xz $wine_gecko_dir
        echo PDB package done:
        echo $(pwd)/$package_basename-pdb.tar.xz
    fi
    ;;
esac

if [ "$pdb" = "no" ]
then
    case $pkg_type in
    unstripped|all)
        echo "make_package: Creating unstripped archive..."

        cd "$build_dir/dist"

        cp -r $wine_gecko_dir $wine_gecko_dir-unstripped

        # This is hackish, but this allows us to prepare both stripped
        # and unstripped packages from the same build.
        if [ "$do_strip" = "yes" ]
        then
            cd $wine_gecko_dir-unstripped
            for f in *.dll; do
                cp ../bin/$f $f
            done
            cd ..
        fi
        tar -cJf $package_basename-unstripped.tar.xz $wine_gecko_dir-unstripped
        echo Package done:
        echo $(pwd)/$package_basename-unstripped.tar.xz
        ;;
    esac
fi
